import Vue from "vue";
import VueRouter from "vue-router";
//import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "",
    component: () => import(/* webpackChunkName: "about" */ "../views/newOrg"),
  },

  {
    path: "/edit_do",
    name: "",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/edit_do.vue"),
  },
  {
    path: "/org",
    name: "",
    component: () => import(/* webpackChunkName: "about" */ "../views/newOrg"),
  },
  {
    path: "/orgold",
    name: "",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/newOrgcopy"),
  },
  {
    path: "/t",
    name: "",
    component: () => import(/* webpackChunkName: "about" */ "../views/test"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
